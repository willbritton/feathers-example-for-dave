'use strict';

const assert = require('assert');
const app = require('../../../src/app');

describe('myData service', function() {
  it('registered the myData service', () => {
    assert.ok(app.service('myData'));
  });
});
