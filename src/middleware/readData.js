'use strict';

module.exports = function(app) {
  return function(req, res, next) {
  	const service = app.service('myData');

  	service.find({}).then((result) => {
  		console.log(result);
  		let response = '';
  		for (let item of result.data) {
  			response += `<tr><td>${item.firstName}</td><td>${item.lastName}</td><td>${item.age}</td></tr>`;
  		}
  		res.send(`<html><body><table><tr><th>First name</th><th>Last name</th><th>Age</th></tr>${response}</body></html>`);
  	}).catch(next);
  };
};
