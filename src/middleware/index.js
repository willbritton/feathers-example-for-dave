'use strict';

const readData = require('./readData');
const myForm = require('./myForm');

const handler = require('feathers-errors/handler');
const notFound = require('./not-found-handler');
const logger = require('./logger');

module.exports = function() {
  // Add your custom middleware here. Remember, that
  // just like Express the order matters, so error
  // handling middleware should go last.
  const app = this;

  app.use('/myform', myForm(app))
  app.use('/data', readData(app))

  app.use(notFound());
  app.use(logger(app));
  app.use(handler());
};
