'use strict';

module.exports = function(app) {
  return function(req, res, next) {
  	const myDataService = app.service('myData'); // Load our new service
  	const formData = req.query; // get form data from request
  	const record = {
  		firstName: formData.firstname,
  		lastName: formData.lastname,
  		age: formData.age,
  	};

  	myDataService.create(record).then(() => {
  		res.send('Thanks for that data - nom noms!');
  		res.send('Thanks for that data - nom noms!');
  	}).catch((err) => {
  		//console.log(err)
  		res.send('Something went wrong' + err);
  	})
  };
};
