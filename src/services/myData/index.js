'use strict';

const service = require('feathers-mongoose');
const myData = require('./myData-model');
const hooks = require('./hooks');

module.exports = function() {
  const app = this;

  const options = {
    Model: myData,
    paginate: {
      default: 5,
      max: 25
    }
  };

  // Initialize our service with any options it requires
  app.use('/myData', service(options));

  // Get our initialize service to that we can bind hooks
  const myDataService = app.service('/myData');

  // Set up our before hooks
  myDataService.before(hooks.before);

  // Set up our after hooks
  myDataService.after(hooks.after);
};
